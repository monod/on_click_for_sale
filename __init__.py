# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .sale import *

__all__ = ['register']

def register():
    Pool.register(
		Sale,
        module='one_click_for_sale', type_='model')
    Pool.register(
		OneClickForSale,
        module='one_click_for_sale', type_='wizard')
    Pool.register(
        module='one_click_for_sale', type_='report')
