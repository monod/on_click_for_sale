# -*- coding: utf-8 -*-
from trytond.wizard import Wizard, StateAction, StateView, StateTransition, \
    Button
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
import logging
from trytond.model import Workflow, ModelView, ModelSQL
from trytond.modules.account.tax import TaxableMixin
from trytond.pyson import Eval

logger = logging.getLogger(__name__)


__all__ = ['OneClickForSale','Sale']

class Sale(Workflow, ModelSQL, ModelView, TaxableMixin):
    'Sale'
    __metaclass__ = PoolMeta
    __name__ = 'sale.sale'
    
    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._buttons.update({
                'draft_to_done': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'readonly': ~Eval('lines', []),
                    },
        })

    
    @classmethod
    @ModelView.button
    def draft_to_done(cls,sales):
        '''Lleva las ventas desde el estado borrador(draft) hasta el estado finalizado(done).
           En estre proceso crea la factura y los movimientos de stock de forma automática,
           además de aprobarlos y contabilizarlos.'''
        
        Invoice = Pool().get('account.invoice')
        Shipment = Pool().get('stock.shipment.out')
        
        if sales:
            for sale in sales:
                if sale.state != 'draft' or len(sale.lines)<1:
                    cls.raise_user_error('La venta{s} no está en estado borrador o no contiene lineas'.foramt(s=sale))
                    return
        else:
            cls.raise_user_error('Error en las venatas seleccionadas')
            return
        
        cls.quote(sales)
        cls.confirm(sales)
        cls.process(sales)
        Transaction().commit()#se graba la informacion para poder acceder a las facturas y los shipments
        sales = cls.browse([x.id for x in sales])#se vuelve a leer para cargar informacion de la base de datos, verificar si es necesario.
        sales = [x for x in sales if (x.state == 'processing' and len(x.lines) >0 ) ]#solo las ventas en borrador
        if len(sales) <= 0:
            cls.raise_user_error('Falló la finalización automática de la(s) venta(s)')
            return
        for sale in sales:
            invoices = sale.invoices
            if invoices:
                for invoice in invoices:
                    if not invoice.invoice_date:
                        invoice.invoice_date = sale.sale_date
                        Invoice.save([invoice])
                Invoice.validate_invoice(invoices)
                Invoice.post(invoices)
            shipments = sale.shipments
            if shipments:
                for shipment in shipments:
                    shipment.effective_date = sale.sale_date
                    Shipment.save([shipment])
                Shipment.assign_wizard(shipments)
                Shipment.pack(shipments)
                Shipment.done(shipments)

class OneClickForSale(Wizard):
    '''Venta en un click.
    
    Procesa una venta en estado de borrador llevandola al estado "procesada",
    creando las Facturas y Movimientos de Stock además contabiliza las facturas
    y finaliza los movimientos de stock y los envios.
    '''
    
    __name__ = 'sale.one_click_for_sale'
    
    start = StateTransition()
    
    def transition_start(self):
        Sale = Pool().get('sale.sale')
        sales = Sale.browse(Transaction().context['active_ids'])
        Sale.draft_to_done(sales)
        return 'end'