# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.one_click_for_sale.tests.test_one_click_for_sale import suite
except ImportError:
    from .test_one_click_for_sale import suite

__all__ = ['suite']
